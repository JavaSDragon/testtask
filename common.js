window.onload = function () {

    const inputEmail = document.getElementById("input_1");
    const inputPassword = document.getElementById("input_2");
    const submitBtn = document.querySelector(".form__singBtn");
    const errPass = document.querySelector(".form__errPass");
    const errEmail = document.querySelector(".form__errEmail");

    function changeClass(elem) {

        const label = elem.nextElementSibling;

        elem.onfocus = function () {
            errPass.style.display = "none";
            errEmail.style.display = "none";
            label.className = "form__tipFocus";
            elem.style.borderColor = "#06b66f";
        }

        elem.onblur = function () {
            elem.style.borderColor = "#1a7a54";
            if (!elem.value.length) {
                label.className = "form__tip";
            }
        }
    }

    changeClass(inputEmail);
    changeClass(inputPassword);

    submitBtn.onclick = function () {

        const agreeCheck = document.querySelector(".form__agreeCheck")

        if (inputEmail.value == "test@email.com") {
            errEmail.style.display = "flex";
            inputEmail.style.borderColor = "#E15433";
            return false;
        }

        if (inputPassword.value.length < 4) {
            errPass.style.display = "block";
            inputPassword.style.borderColor = "#E15433";
            return false;
        }

        if (!agreeCheck.checked) {
            return false;
        }

    }
}